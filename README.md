# Welcome to the Road Procedural Generation Tool!

This document will explain how to use the tool as well as why it has been developed.

# Functionality

This tool is mainly developed to generate a road in a city. The buildings that are added is an extra, and more assets can be generated to the user's liking by using these scripts. The tool uses a texture where the road is generated as black pixels, and then the black patterns on the texture is scanned to determine where and what type of road piece will be spawned.

# How do I use it?

When a Unity scene is opened, launch the unitypackage to import it into your project. After it's imported, you'll see a couple of folders with prefabs in them, as well as some scripts. The 2 Major scripts that are used for the road generation is named: RoadGeneration and RoadSpawn. 

RoadGeneration is responsible for the texture generation, whereas RoadSpawn is responsible for the Instantiation of the road prefabs into the 3D world(The plot generation locations are processed here as well). 

If you want to use the tool, drag the "RoadGenerator" prefab into your unity scene. You'll see that RoadGeneration is already assigned to the object. Under the "Settings" header on this script, you'll be able to change the size of the texture/ map, the amount of initial black pixels spawned on the texture, the minimum and maximum length of the roads, as well as the depth of the road generation. The depth determines the amount of intersections spawned after the first initial intersection. 